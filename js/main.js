$(document).ready(function() {

  var imageData;

  /** 
    * UI Elements
    */
  var ui = {
    wrapper: $(".gallery"),
    previousButton: $("#prev"),
    nextButton: $("#next"),
    imageTitle: $("#imageTitle"),
    imageDescription: $("#imageDescription"),
    imageContainer: $(".images")
  };



  /** 
    * Update Image Labels
    * ----
    * This function should set the #imageTitle and
    * #imageDescription elements to the title and
    * description of the .current visible image.
    */
  function updateLabel() {
    var currentImage = $('.gallery .current');
    var prevImage = currentImage.prev();
    var nextImage = currentImage.next();

	if(nextImage.length === 0) {
        nextImage = $('.gallery .image').first();
    } 
    if(prevImage.length === 0) {
    	prevImage = $('.gallery .image').last();
    }

    $("#prev").attr("title", prevImage[0].firstChild.getAttribute("title"));
    $("#next").attr("title", nextImage[0].firstChild.getAttribute("title"));

    $("#imageTitle").text(currentImage[0].firstChild.getAttribute("title"));
    $("#imageDescription").text(currentImage[0].firstChild.getAttribute("alt"));
   };

  /** 
    * Data Load Event handler
    * ----
    * Call this function when the images JSON data
    * has loaded. This function should:
    *   1. Create the DOM element for each image 
    *      and append it to the imageContainer element
    *   2. Assign the appropriate classes to the 
    *      image DOM elements to display the first image
    *   3. Call the updateLabel() function
    *   4. Remove the "loading" class from the gallery 
    *      wrapper.
    */
  function onDataLoad(data) { 

  	$('.gallery').removeClass("loading");
  	var jsonData = JSON.parse(data);
  	for (i=0; i<jsonData.length; i++){
  		var title = jsonData[i]["title"];
  		var source = jsonData[i]["src"];
  		var desc = jsonData[i]["description"];
  		$('.images').append("<div class='image'><img title='"+title+"' src='"+source+"' alt='"+desc+"' /></div>");
  		if (i===0){
  			$('.image').addClass("bounceInLeft current");
  		} else {
  			$('.image').addClass("right");
  		}
  	}

  	updateLabel();
  };


 
  /**
    * AJAX request
    * ----
    * Add an AJAX request to get the /data/images.json
    * file here, and call onDataLoad on success.
	*/
  var request;
  if (window.XMLHttpRequest){
    request = new XMLHttpRequest();
  } 
  else { //for IE5 & IE6
    request = new ActiveXObject("Microsoft.XMLHTTP");
  }
  request.open("GET","/data/images.json",true);
  request.send();
  request.onreadystatechange=function(){
  	if(request.readyState==4 && request.status==200){
  		onDataLoad(request.responseText);
  	}
  }



  /** 
    * Event Handlers
    * ----
    * Place all code that is triggered by
    * user interaction here.
    */
  function onPreviousClick() {
    switchImage("left");
  };

  function onNextClick() {
    switchImage("right");
  };

  // bind event handlers to their actions
  ui.previousButton.click(onPreviousClick);
  ui.nextButton.click(onNextClick);

  /** 
    * Switch image
    * ----
    * Gets the next or previous .image DOM element
    * relative to the .current image, then sets the
    * classes to make that element visible.
    */
  function switchImage(dir) {
    var currentImage = $('.gallery .current');
    var nextImage;
    switch (dir) {
      case "left":      
        nextImage = currentImage.prev();
        if(nextImage.length === 0) {
          nextImage = $('.gallery .image').last();
        }
        currentImage.attr('class', 'image bounceOutRight');
        nextImage.attr('class', 'image current bounceInLeft');
        break;
      case "right":
        nextImage = currentImage.next();
        if(nextImage.length === 0) {
          nextImage = $('.gallery .image').first();
        }
        currentImage.attr('class', 'image bounceOutLeft');
        nextImage.attr('class', 'image current bounceInRight');
        break;
    }
    updateLabel();
  };

});